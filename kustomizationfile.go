package main

import (
	"io/ioutil"

	"github.com/ghodss/yaml"
	"github.com/kubernetes-sigs/kustomize/pkg/types"
)

type KustomizationFile types.Kustomization

func (k *KustomizationFile) UsedFiles() []string {
	files := []string{}
	files = append(files, k.Resources...)
	for _, patch := range k.PatchesStrategicMerge {
		files = append(files, string(patch))
	}
	for _, patch := range k.PatchesJson6902 {
		files = append(files, patch.Path)
	}
	return files
}

func ParseKustomizationFile(path string) (*KustomizationFile, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}
	k := &KustomizationFile{}
	err = yaml.Unmarshal(b, &k)
	if err != nil {
		return nil, err
	}
	return k, nil
}
