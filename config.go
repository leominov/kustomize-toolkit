package main

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

const (
	DefaultConfigFile        = "config.yaml"
	DefaultKustomizationFile = "kustomization.yaml"
)

type Config struct {
	SpecsDirectory string   `yaml:"specsDirectory"`
	CheckExclude   []string `yaml:"checkExclude"`
}

func DefaultConfig() *Config {
	return &Config{
		SpecsDirectory: "kustomize",
		CheckExclude: []string{
			".*/meta.yaml",
			".*/dependencies.yaml",
		},
	}
}

func LoadConfig(path string) *Config {
	config := DefaultConfig()
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return config
	}
	err = yaml.Unmarshal(b, &config)
	if err != nil {
		return config
	}
	return config
}
