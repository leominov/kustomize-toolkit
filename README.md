# Kustomize Toolkit

## Check

Check that all files in kustomize directory defined in kustomization file.

### Usage

```
$ kustomize-toolkit check directory/
+ directory/
  exists, but not defined: overlays/common/poddisruptionbudget.yaml, overlays/common/strategy.yaml
Failed. See messages above
exit status 2
```
