package main

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"regexp"
	"strings"
)

func findFiles(root string) ([]string, []string, error) {
	var (
		kustomizationFiles []string
		otherFiles         []string
	)
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			return nil
		}
		if info.Name() == DefaultKustomizationFile {
			kustomizationFile, err := ParseKustomizationFile(path)
			if err != nil {
				return err
			}
			usedFiles := kustomizationFile.UsedFiles()
			for _, usedFile := range usedFiles {
				f := filepath.Join(filepath.Dir(path), usedFile)
				kustomizationFiles = append(kustomizationFiles, f)
			}
		} else {
			otherFiles = append(otherFiles, path)
		}
		return nil
	})
	return kustomizationFiles, otherFiles, err
}

func checkIncludeCmd(args []string) error {
	var failed bool
	if len(args) == 0 {
		args = []string{"./"}
	}
	for _, arg := range args {
		info, err := os.Stat(arg)
		if err != nil {
			return err
		}
		if !info.IsDir() {
			continue
		}
		workDir, err := filepath.Abs(arg)
		if err != nil {
			return err
		}
		kustomizeDir := filepath.Join(workDir, config.SpecsDirectory)
		if !isDirExists(kustomizeDir) {
			continue
		}
		fmt.Println("+", arg)
		kustomizationFiles, otherFiles, err := findFiles(kustomizeDir)
		if err != nil {
			return err
		}
		diffFiles := diffLists(otherFiles, kustomizationFiles)
		filteredFiles := checkExcludeFilter(diffFiles)
		filteredFiles = removePrefixPath(filteredFiles, kustomizeDir+"/")
		if len(filteredFiles) > 0 {
			failed = true
			fmt.Println("  exists, but not defined:", strings.Join(filteredFiles, ", "))
		}
	}
	if failed {
		return errors.New("Failed. See messages above")
	}
	return nil
}

func removePrefixPath(files []string, path string) []string {
	for i, file := range files {
		files[i] = strings.TrimPrefix(file, path)
	}
	return files
}

func checkExcludeFilter(files []string) []string {
	var (
		match   bool
		result  []string
		regexps []*regexp.Regexp
	)
	if len(config.CheckExclude) == 0 {
		return files
	}
	for _, re := range config.CheckExclude {
		regexps = append(regexps, regexp.MustCompile(re))
	}
	for _, file := range files {
		match = false
		for _, re := range regexps {
			if re.MatchString(file) {
				match = true
				break
			}
		}
		if !match {
			result = append(result, file)
		}
	}
	return result
}
