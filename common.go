package main

import "os"

func isDirExists(dir string) bool {
	if _, err := os.Stat(dir); os.IsNotExist(err) {
		return false
	}
	return true
}

func diffLists(a, b []string) []string {
	var (
		result []string
		match  bool
	)
	for _, fileA := range a {
		match = false
		for _, fileB := range b {
			if fileA == fileB {
				match = true
				break
			}
		}
		if !match {
			result = append(result, fileA)
		}
	}
	return result
}
