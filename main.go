package main

import (
	"errors"
	"fmt"
	"os"
)

var (
	config *Config
)

func parseArgs(args []string) (cmd string, cmdArgs []string, err error) {
	if len(args) <= 1 {
		err = errors.New("Command must be specified")
		return
	}
	cmd = args[1]
	if len(args) > 1 {
		cmdArgs = args[2:]
	}
	return
}

func runCmd(cmd string, cmdArgs []string) error {
	switch cmd {
	case "check":
		return checkIncludeCmd(cmdArgs)
	}
	return errors.New("Incorrect command")
}

func realMain() int {
	config = LoadConfig(DefaultConfigFile)
	args := os.Args
	cmd, cmdArgs, err := parseArgs(args)
	if err != nil {
		fmt.Println(err)
		return 1
	}
	err = runCmd(cmd, cmdArgs)
	if err != nil {
		fmt.Println(err)
		return 2
	}
	return 0
}

func main() {
	result := realMain()
	if result > 0 {
		os.Exit(result)
	}
}
